# Mini projects

## Objectives

This mini-project aims to allow the students to practice their programming
skills in Python in a project of their choice.

The last practice session will be dedicated to these projects but you can
already think about it and even start to work.

## Description

The choice of the topic is completely free as long as the student hands a
personal job which relies on Python programming. If possible, this mini-project
can be coupled with projects from other classes followed by the student in this
semester.

*Note 1: Students are advised to discuss with one of the teachers about their
topic before starting. You can open a dedicated issue in
https://gricad-gitlab.univ-grenoble-alpes.fr/meige-legi/scientific-computing-m2-efm.*

*Note 2: An indicative personal workload for the mini-project is about 6 hours
in addition of the last practice session. This information should help the
student to choose a topic which will not require an unreasonable time
investment.*

## Evaluation

The students have to put the code in a Gitlab project hosted on
<https://gricad-gitlab.univ-grenoble-alpes.fr>. To ease the evaluation, we ask
you to use a particular name: `miniproject-scientific-computing2020` (so that
the full address is
`https://gricad-gitlab.univ-grenoble-alpes.fr/your-username/miniproject-scientific-computing2020`).

You will have to post the address of the Gitlab project of your mini-project in
a message in the [issue
#1](./-/issues/1).

Then, the evaluation of these mini-projects will be done through a short oral
sessions (~ 10 minutes) where the students will be asked to present their work.
It will corresponds to half of the practical session grade.

## Examples of mini-projects

### Monte Carlo Integration

Read about the Monte Carlo integration numerical method (e.g.
https://en.wikipedia.org/wiki/Monte_Carlo_integration). In a similar way to
what will be done in the practice session, investigate and illustrate pros and
cons of this method. How does it compare to other methods that you know about ?
In which situation can it be useful ?

### Animation of particles in a box

Use Matplotlib to create an (interactive) animation of colliding particles in a
box. An extension to this project could be to link this animation to the ideal
gas law.

### A Machine Learning Problem

Choose a ML classical problem (e.g. from the Kaggle database:
https://www.kaggle.com/competitions) and try to tackle the problem. This is
related to topics that will be treated in one of the practice session.
Dynamical Website
