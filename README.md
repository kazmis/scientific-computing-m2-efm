# Scientific Computing (Master 2 EFM)

## Content

During sessions 1, 2 and 4 (9 hours), we will present an introduction on
scientific computing and programming:

- Computers to compute (CPU, memories, GPU, clusters)
- Numbers in computers
- Programming languages
- Difference open-source / close-source
- Operating systems and importance of Linux for scientific computing
- Install a good environment, example of Conda
- Versioning and Gitlab (https://gricad-gitlab.univ-grenoble-alpes.fr)

We will study some basics on Linux and Python for scientific computing.

The personal project part will be presented during session 3. Sessions 3, 5, 6
and 7 will be dedicated to practical exercises on (3) Computation of integrals,
(5) Finite Difference methods, (6) Gradient descent and (7) Machine learning.

We will work together on your personal projects during session 8.

## The teachers

- Pierre Augier: researcher at [LEGI](www.legi.grenoble-inp.fr) studying
geophysical turbulence with experiments and numerical simulations. Maintainer
of the [FluidDyn project](https://fluiddyn.readthedocs.io).

- Enzo Le Bouedec: PhD at LEGI on "Pollution in the Grenoble valley: a
weather-type approach".

## Setup the environment for this course

See the file
[install.md](https://gricad-gitlab.univ-grenoble-alpes.fr/meige-legi/scientific-computing-m2-efm/-/blob/master/install.md).
Note that you can copy/paste commands!

## Clone this repository

Clone the repository with Mercurial (and the extension hg-git, as explained
[here](https://fluiddyn.readthedocs.io/en/latest/mercurial_heptapod.html)):

```
hg clone https://gricad-gitlab.univ-grenoble-alpes.fr/meige-legi/scientific-computing-m2-efm.git
```

or with ssh (so you need to create a ssh key and copy the public key on
https://gricad-gitlab.univ-grenoble-alpes.fr):

```
hg clone git@gricad-gitlab.univ-grenoble-alpes.fr:meige-legi/scientific-computing-m2-efm.git
```

## Play with (or display) the notebooks

To modify the notebooks:

```
cd ipynb
jupyter-lab
```

To see the presentations made from the notebooks:

```
make presentations
make serve
```

## Slides on the web

The slides of this course are also hosted
[here](https://meige-legi.gricad-pages.univ-grenoble-alpes.fr/scientific-computing-m2-efm/).

If you feel that you need more advanced content, you can work on this [Python
HPC
training](https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/training-hpc)
(the associated slices are
[here](https://python-uga.gricad-pages.univ-grenoble-alpes.fr/training-hpc/)).
